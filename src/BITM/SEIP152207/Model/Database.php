<?php
namespace App\Model;

use PDO;
use PDOException;


class Database
{
    public  $DBH;
    public  $host="localhost";
    public $dbname="relationb36";
    public $username = "root";
    public $password = "";

    public function __construct()
    {
        try {

            $this->DBH = new PDO("mysql:host=localhost;dbname=$this->dbname", $this->username, $this->password);
            echo "succesfully connected <br>";

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

